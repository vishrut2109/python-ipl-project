import csv
import json


def main():
    def won_toss_won_match(parsed_data_matches):
        result={}
        try:
            for match in parsed_data_matches:
                toss_winner = match['toss_winner']
                winner = match['winner']
                if (toss_winner == winner):
                    if winner in result.keys():
                        result[winner] += 1
                    else:
                        result[winner] = 1
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

        return result

    try:
        with open('../data/matches.csv','r')as file:
            parsed_data_matches=csv.DictReader(file)
            result=won_toss_won_match(parsed_data_matches)
            print(result)
            try:
                with open('../public/output/5-won-toss-won-match.json','w')as file1:
                    file1.write(json.dumps(result))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")

if(__name__=='__main__'):
    main()