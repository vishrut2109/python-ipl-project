import csv
import json


def main():
    player_dismissals = {}

    def get_player_dismissals(parsed_data_deliveries):
        try:
            for delivery in parsed_data_deliveries:
                player_dismissed = delivery['player_dismissed']
                bowler = delivery['bowler']
                if (player_dismissed != ''):
                    if player_dismissed in player_dismissals:
                        if bowler in player_dismissals[player_dismissed]:
                            player_dismissals[player_dismissed][bowler] += 1
                        else:
                            player_dismissals[player_dismissed][bowler] = 1
                    else:
                        player_dismissals[player_dismissed] = {}
                        player_dismissals[player_dismissed][bowler] = 1
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

        try:
            for dismissal in player_dismissals:
                max_key = max(player_dismissals[dismissal], key=player_dismissals[dismissal].get)
                max_value = player_dismissals[dismissal][max_key]
                player_dismissals[dismissal] = {max_key: max_value}
        except Exception as error:
            print(f"Error occurred: {error}")

        return player_dismissals

    try:
        with open('../data/deliveries.csv', 'r') as file:
            parsed_data_deliveries = csv.DictReader(file)
            player_dismissals = get_player_dismissals(parsed_data_deliveries)
            print(player_dismissals)
            try:
                with open('../public/output/8-highest-dismissals.json', 'w') as file1:
                    file1.write(json.dumps(player_dismissals))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if (__name__ == '__main__'):
    main()
