import csv
import json


def main():
    batsman_name = "V Kohli"
    runs_scored_balls_faced = {}
    season_match_ids = {}

    def get_season_match_ids(parsed_data_matches):
        try:
            for match in parsed_data_matches:
                season = match['season']
                id = match['id']
                if season in season_match_ids.keys():
                    season_match_ids[season].append(id)
                else:
                    season_match_ids[season] = []
                    season_match_ids[season].append(id)
        except Exception as error:
            print(f"Error occurred: {error}")

        return season_match_ids

    def get_strike_rate_batsman(parsed_data_deliveries):
        try:
            for delivery in parsed_data_deliveries:
                batsman = delivery['batsman']
                batsman_runs = delivery['batsman_runs']

                match_id = delivery['match_id']
                delivery_season = ""
                if (batsman == batsman_name):
                    #  to find out season from using match id from deliveries data
                    for season in season_match_ids:
                        if match_id in season_match_ids[season]:
                            delivery_season = season
                            break

                    #  storing batsman runs at index 0 and balls faced at index1 in runs_scored_balls_faced for a given season
                    if delivery_season in runs_scored_balls_faced.keys():
                        runs_scored_balls_faced[delivery_season][0] += int(batsman_runs)
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            runs_scored_balls_faced[delivery_season][1] += 1
                        else:
                            pass
                    else:
                        runs_scored_balls_faced[delivery_season] = []
                        runs_scored_balls_faced[delivery_season].append(int(batsman_runs))
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            runs_scored_balls_faced[delivery_season].append(1)
                        else:
                            runs_scored_balls_faced[delivery_season].append(0)

                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

        try:
            # we have balls faced and runs scored by batsman for each season now we will find strike rate
            for season in runs_scored_balls_faced:
                runs_scored_balls_faced[season] = round(
                    (runs_scored_balls_faced[season][0] / runs_scored_balls_faced[season][1]) * 100, 2)
        except Exception as error:
            print(f"Error occurred: {error}")

        return runs_scored_balls_faced

    try:
        with open('../data/matches.csv', 'r') as file:
            parsed_data_matches = csv.DictReader(file)
            season_match_ids = get_season_match_ids(parsed_data_matches)
    except Exception as error:
        print(f"Error occurred: {error}")

    try:
        with open('../data/deliveries.csv', 'r') as file1:
            parsed_data_deliveries = csv.DictReader(file1)
            strike_rate_batsman = get_strike_rate_batsman(parsed_data_deliveries)
            print(strike_rate_batsman)
            try:
                with open('../public/output/7-strike-rate-batsman-each-season.json', 'w') as file2:
                    file2.write(json.dumps(strike_rate_batsman))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if __name__ == '__main__':
    main()
