import csv
import json


def main():
    match_ids_2015 = []

    def find_match_ids_2015(parsed_data_matches):
        try:
            for match in parsed_data_matches:
                season = match['season']
                id = match['id']
                if (int(season) == 2015):
                    match_ids_2015.append(id)
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

    def get_top_bowlers(parsed_data_deliveries):
        bowler_economy_data = {}
        try:
            for delivery in parsed_data_deliveries:
                bowler = delivery['bowler']
                total_runs = int(delivery['total_runs'])
                if delivery['match_id'] in match_ids_2015:
                    if bowler in bowler_economy_data:
                        bowler_economy_data[bowler][0] += total_runs
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            bowler_economy_data[bowler][1] += 1
                        else:
                            pass

                    else:
                        bowler_economy_data[bowler] = []
                        bowler_economy_data[bowler].append(total_runs)
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            bowler_economy_data[bowler].append(1)
                        else:
                            bowler_economy_data[bowler].append(0)
        except Exception as error:
            print(f"Exception occurred: {error}")

        try:
            for bowler in bowler_economy_data:
                bowler_economy_data[bowler] = bowler_economy_data[bowler][0] / (bowler_economy_data[bowler][1] / 6)

            sorted_data = sorted(bowler_economy_data.items(), key=lambda x: x[1])[:10]
            result = {}
            for bowler in sorted_data:
                result[bowler[0]] = bowler[1]
        except Exception as error:
            print(f"Error occurred: {error}")

        return result

    try:
        with open('../data/matches.csv', 'r') as file:
            parsed_data_matches = csv.DictReader(file)
            find_match_ids_2015(parsed_data_matches)
    except Exception as error:
        print(f"Error occurred: {error}")

    try:
        with open('../data/deliveries.csv', 'r') as file1:
            parsed_data_deliveries = csv.DictReader(file1)
            result = get_top_bowlers(parsed_data_deliveries)
            print(result)
            try:
                with open('../public/output/4-top-economical-bowlers.json', 'w') as file2:
                    file2.write(json.dumps(result))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if (__name__ == '__main__'):
    main()
