import csv
import json


def main():
    def matches_per_year(parsed_data):
        matches_in_year = {}
        try:
            for row in parsed_data:
                season = row['season']
                if season in matches_in_year.keys():
                    matches_in_year[season] = matches_in_year[season] + 1
                else:
                    matches_in_year[season] = 1
        except Exception as error:
            print(f"Error occurred: {error}")

        return matches_in_year

    try:
        with open('../data/matches.csv', 'r') as file:

            # create a CSV reader object
            parsed_data = csv.DictReader(file)

            # calling function to calculate Number of matches played per year for all the years in IPL.
            matches_in_year = matches_per_year(parsed_data)
            matches_in_year_json = json.dumps(matches_in_year)
            print(matches_in_year_json)
            try:
                with open("../public/output/1-matches-per-year.json", 'w') as file1:
                    # Write data to the file
                    file1.write(matches_in_year_json)
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if (__name__ == "__main__"):
    main()
