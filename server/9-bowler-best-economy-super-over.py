import csv
import json


def main():
    def get_best_economy_bowler(parsed_data_deliveries):
        super_over_econnomies = {}

        try:
            for delivery in parsed_data_deliveries:

                if (int(delivery['is_super_over']) == 1):
                    bowler = delivery['bowler']
                    total_runs = int(delivery['total_runs'])

                    if bowler in super_over_econnomies:
                        super_over_econnomies[bowler][0] += total_runs
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            super_over_econnomies[bowler][1] += 1
                        else:
                            pass
                    else:
                        super_over_econnomies[bowler] = []
                        super_over_econnomies[bowler].append(total_runs)
                        if (int(delivery['wide_runs']) == 0 and int(delivery['noball_runs']) == 0 and int(
                                delivery['bye_runs']) == 0 and int(delivery['legbye_runs']) == 0):
                            super_over_econnomies[bowler].append(1)
                        else:
                            super_over_econnomies[bowler].append(0)
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

        try:
            for bowler in super_over_econnomies:
                super_over_econnomies[bowler] = super_over_econnomies[bowler][0] / (
                        super_over_econnomies[bowler][1] / 6)
            max_key = min(super_over_econnomies, key=super_over_econnomies.get)
            max_value = super_over_econnomies[max_key]
        except Exception as error:
            print(f"Error occurred: {error}")

        return {max_key: max_value}

    try:
        with open('../data/deliveries.csv', 'r') as file:
            parsed_data_deliveries = csv.DictReader(file)
            best_economy_bowler = get_best_economy_bowler(parsed_data_deliveries)
            print(best_economy_bowler)
            try:
                with open('../public/output/9-bowler-best-economy-super-over.json', 'w') as file1:
                    file1.write(json.dumps(best_economy_bowler))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if (__name__ == '__main__'):
    main()
