import csv
import json


def main():
    def matches_won_per_team_per_year(parsed_data):
        result = {}
        try:
            for row in parsed_data:
                season = row['season']
                winner = row['winner']
                if season in result.keys():
                    if winner in result[season].keys():
                        result[season][winner] += 1
                    else:
                        result[season][winner] = 1
                else:
                    result[season] = {}
                    result[season][winner] = 1
        except Exception as error:
            print(f"Error occurred: {error}")

        return result

    try:
        with open('../data/matches.csv', 'r') as file:
            parsed_data = csv.DictReader(file)

            result = matches_won_per_team_per_year(parsed_data)
            result_json = json.dumps(result)
            print(result)
            try:
                with open('../public/output/2-matches-won-per-team-per-year.json', 'w') as file1:
                    file1.write(result_json)
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")

    except Exception as error:
        print(f"Error occurred: {error}")






if(__name__=='__main__'):
    main()

