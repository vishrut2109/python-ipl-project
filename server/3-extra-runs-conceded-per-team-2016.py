import csv
import json


def main():
    match_id_list = []

    def get_match_ids_2016(parsed_data_matches):

        try:
            for match in parsed_data_matches:
                season = match['season']
                match_id = match['id']
                if (season == '2016'):
                    match_id_list.append(match_id)
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

    def get_extra_runs_conceded(parsed_data_deliveries):
        extra_runs_result = {}

        try:
            for delivery in parsed_data_deliveries:
                bowling_team = delivery['bowling_team']
                extra_runs = delivery['extra_runs']
                if (delivery['match_id'] in match_id_list):
                    if bowling_team in extra_runs_result.keys():
                        extra_runs_result[bowling_team] += int(extra_runs)
                    else:
                        extra_runs_result[bowling_team] = int(extra_runs)
                else:
                    pass
        except Exception as error:
            print(f"Error occurred: {error}")

        return extra_runs_result

    try:
        with open('../data/matches.csv', 'r') as file:
            parsed_data_matches = csv.DictReader(file)
            get_match_ids_2016(parsed_data_matches)
    except Exception as error:
        print(f"Error occurred: {error}")

    try:
        with open('../data/deliveries.csv', 'r') as file1:
            parsed_data_deliveries = csv.DictReader(file1)
            extra_runs_result = get_extra_runs_conceded(parsed_data_deliveries)
            print(extra_runs_result)

            try:
                with open('../public/output/3-extra-runs-conceded-per-team-2016.json', 'w') as file2:
                    file2.write(json.dumps(extra_runs_result))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if (__name__ == '__main__'):
    main()
