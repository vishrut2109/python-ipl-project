import csv
import json


def main():
    def get_most_times_player_of_match(parsed_data_matches):
        player_of_match_all_season={}

        #  try to get all the player of match with their count for all seasons in one dictionary
        try:
            for match in parsed_data_matches:
                player_of_match = match['player_of_match']
                season = match['season']
                if season in player_of_match_all_season.keys():
                    if player_of_match in player_of_match_all_season[season].keys():
                        player_of_match_all_season[season][player_of_match] += 1
                    else:
                        player_of_match_all_season[season][player_of_match] = 1
                else:
                    player_of_match_all_season[season] = {}
                    player_of_match_all_season[season][player_of_match] = 1
        except Exception as error:
            print(f"Error occurred: {error}")


        # will try to get max value or most times of player of match from the player of match data
        most_times_player_of_match={}
        try:
            for season in player_of_match_all_season:
                max = 0
                max_player = ""
                for player, number_of_times in player_of_match_all_season[season].items():
                    if (number_of_times > max):
                        max = number_of_times
                        max_player = player
                most_times_player_of_match[season] = {}
                most_times_player_of_match[season][max_player] = max
        except Exception as error:
            print(f"Error occurred: {error}")

        return most_times_player_of_match


    try:
        with open('../data/matches.csv', 'r') as file:
            parsed_data_matches = csv.DictReader(file)
            result = get_most_times_player_of_match(parsed_data_matches)
            print(result)
            try:
                with open('../public/output/6-most-times-player-of-match.json', 'w') as file1:
                    file1.write(json.dumps(result))
            except Exception as error:
                print(f"Error occurred: {error}")
            else:
                print("File written successfully")
    except Exception as error:
        print(f"Error occurred: {error}")


if(__name__=='__main__'):
    main()